var repository = require('./repository').Repository;
var suggestionModel = require('./models/suggestion-model');
var suggestionRepository = new repository(suggestionModel);

var user = require('./models/user');
var userRepository = new repository(user);

var middleware = require('./middleware');
var isLoggedInOnly = middleware.isLoggedInOnly;
var notLoggedInOnly = middleware.notLoggedInOnly;

module.exports = function (app) {
    var jsonfile = require('jsonfile')
    var file = './public/themeState.json';

    app.get('/admin/:page', function (req, res) {
        console.log(req.adminAuthorized);
        if (req.adminAuthorized) {
            req.session.page = req.params.page;
            res.sendfile('./public/backend/index.html');
        }
    });

    app.get('/', function (req, res) {
        if (req.isAuthenticated()) {
            if (req.user.refId) {
                res.render('profile.pug',{user:req.user});
            }
            else {
                res.redirect('/completeInfo');
            }
        }
        else {
            res.render('index.pug');
        }
    });

    app.post('/addsuggestion', function (req, res) {
        console.log(req.body);
        var su = new suggestion({});
        suggestionRepository.add(req.body, function (err) {
            if (err) {
                res.send(err);
            } else {
                res.send(200);
            }
        })
    });

    app.get('/completeInfo', isLoggedInOnly, function (req, res) {
        res.render('completeInfo.pug',{user:req.user});
    });

    app.get('/completeInfo/charity', isLoggedInOnly, function (req, res) {
        res.render('charity.pug'),{user:req.user};
    }); 

    app.get('/completeInfo/donator', isLoggedInOnly, function (req, res) {
      
        res.render('donator.pug',{user:req.user});
    });

    app.get('/completeInfo/volunteer', isLoggedInOnly, function (req, res) {
        res.render('volunteer.pug',{user:req.user});
    });

    app.get('/suggest', function (req, res) {
        res.render('suggest.pug',{user:req.user});
    });

    app.get('/app/setting/get', function (req, res) {
        jsonfile.readFile(file, function (err, obj) {
            console.dir(obj);
            res.send(obj);
        });
    });

    app.post('/app/setting/edit', function (req, res) {
        jsonfile.writeFile(file, req.body, function (err) {
            console.error(err);
            res.send(err);
        });
    });
}