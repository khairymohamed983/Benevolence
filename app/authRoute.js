module.exports = function (app) {
    var passport = require('passport');
 
    app.get('/logout' , function (req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/login',notLoggedIn, function (req, res) {
        res.render('login.pug');
    });

    app.post('/login',notLoggedIn, passport.authenticate('local-login', {
        successRedirect: '/',  
        failureRedirect: '/login',  
        failureFlash: true  
    }));

    app.get('/signup' ,notLoggedIn, function (req, res) {
        res.render('register.pug');
    });

    app.post('/signup',notLoggedIn, passport.authenticate('local-signup', {
        successRedirect: '/',  
        failureRedirect: '/signup',  
        failureFlash: true  
    }));

    // facebook -------------------------------
    app.get('/auth/facebook',notLoggedIn, passport.authenticate('facebook', { scope: 'public_profile' }));
    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback', notLoggedIn,
        passport.authenticate('facebook', {
            successRedirect: '/',
            failureRedirect: '/'
        }));


    // twitter --------------------------------
    app.get('/auth/twitter',notLoggedIn, passport.authenticate('twitter', { scope: 'email' }));
    // handle the callback after twitter has authenticated the user
    app.get('/auth/twitter/callback',notLoggedIn,
        passport.authenticate('twitter', {
            successRedirect: '/',
            failureRedirect: '/'
        }));


    // google ---------------------------------
    app.get('/auth/google',notLoggedIn, passport.authenticate('google', { scope: ['profile', 'email'] }));
    // the callback after google has authenticated the user
    app.get('/auth/google/callback',notLoggedIn,
        passport.authenticate('google', {
            successRedirect: '/',
            failureRedirect: '/'
        }));

};

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}

function notLoggedIn(req, res, next) {
    if (!req.isAuthenticated())
        return next();

    res.redirect('/');
}
