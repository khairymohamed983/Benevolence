///  Seed Data here after connecting to the Database
module.exports = function (app) {
    // Initialize new User Repository
    var User = require('./models/user');
    var Repository = require('./repository').Repository;
    var userRepository = new Repository(User);

    // Insert Default User if Not Exist
    userRepository.find({
        email: 'admin'
    }, function (docs, err) {
        if (!docs.length) {
            var user = new User({
                displayName: 'Admin',
                email: 'admin',
                password: 'admin',
                isAdmin: true
            });
            userRepository.add(user, function (err, result) {
                if (err) {
                    console.log(err);
                }
            });
        }
    });

    userRepository.find({
        email: 'ahmed@mail.com'
    }, function (docs, err) {
        if (!docs.length) {
            var user = new User({
                displayName: 'Ahmed',
                email: 'ahmed@mail.com',
                password: 'ahmed',
                isAdmin: false
            });
            userRepository.add(user, function (err, result) {
                if (err) {
                    console.log(err);
                }
            });
        }
    });
}
