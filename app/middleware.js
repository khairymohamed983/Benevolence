var isLoggedInOnly =  function isLoggedIn(req, res, next) {
    // if (req.isAuthenticated())
        return next();
    // res.redirect('/');
};

var notLoggedInOnly = function notLoggedIn(req, res, next) {
    if (!req.isAuthenticated())
        return next();
    res.redirect('/');
};

var infoCompleted = function infoCompleted(req, res, next) {
    if (req.isAuthenticated())
        return next();
    else
        res.redirect('/');

    if (req.user.refId)
        return next();

    res.redirect('/completeInfo');
};

module.exports.isLoggedInOnly = isLoggedInOnly;
module.exports.notLoggedInOnly = notLoggedInOnly;
module.exports.infoCompleted = infoCompleted;
