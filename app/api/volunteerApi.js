var repository=require('../repository').Repository;
var volunteerModel=require('../models/volunteer-model');
var volunteerRepository=new repository(volunteerModel);
var moment=require('moment');


var multer=require('multer');
var path=require('path');
var uploadPath=path.resolve('public/frontend/uploadimages/');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, uploadPath)
  },
  filename: function (req, file, cb) {
      cb(null, file.originalname);
  }
});
var upload=multer({storage: storage});

module.exports = function (app) {

  app.post('/volunteer_info',upload.single('profileimage'),function (req,res) {
    console.log(req.body.dateofbirth);
    var obj=req.body;
    var newVolunteer={
           firstname:req.body.firstname,
           lastname:req.body.lastname,
           phone:req.body.phone,
           dateofbirth:req.body.dateofbirth,
           university:req.body.university,
           studyfield:req.body.study,
           job:req.body.job,
           skills:req.body.skills,
           address:req.body.address,
           bio:req.body.bio,
           dateofbirth:req.body.date
    };
        if (req.file) {
            newVolunteer.profileimage=req.file.originalname;
        }else{
            newVolunteer.profileimage="default.png";
        }

    var volunt = new volunteerModel(newVolunteer);

    volunteerRepository.add(volunt,function(err){
        if (err) {
            res.send(err);
        } else {
            res.render("volunteer-profile");
        }
    })



  });
  app.get('/vol_api/allvols',function (req,res) {
    console.log("vol in");
          volunteerRepository.find({},function (callback,err) {
            if (err) {
              console.log(err);
            }else if (callback) {
              res.send(callback);
            }
          });
  });
  app.post('/vol_api/deleteVols',function (req,res) {
          var id = req.param('id');
          console.log(id);
        volunteerRepository.remove({
            _id: id
              }, function(err) {
            if (err) {
                res.send(500);
              } else {
                res.json({ message: 'The Volunteer Successfully Deleted ' });

            }
        });
  })



};
