var moment = require('moment');
var jwt = require('jwt-simple');
var middleware = require('./authMiddleware');
var ensureAuthenticated = middleware.authMiddleware;
var createJWT = middleware.jwtgen;

/// Users End Point
module.exports = function(app) {
    // Initialize new Course Repository
    var AdminUser = require('../models/adminUser');
    var Repository = require('../repository').Repository;
    var adminUserRepository = new Repository(AdminUser);
    /*
    |--------------------------------------------------------------------------
    | GET /api/me
    |--------------------------------------------------------------------------
    */
    app.get('/api/me', ensureAuthenticated, function(req, res) {
        AdminUser.findById(req.adminUser, function(err, adminUser) {
            res.send(adminUser);
        });
    });

    app.get('/api/all', ensureAuthenticated, function(req, res) {
        AdminUser.find(function(err, data) {
            if (err) {
                res.send(err);
            } else {
                res.send(data);
            }
        });
    });
    /*
     |--------------------------------------------------------------------------
     | PUT /api/me
     |--------------------------------------------------------------------------
     */
    app.put('/api/me', ensureAuthenticated, function(req, res) {
        AdminUser.findById(req.adminUser, function(err, adminUser) {
            if (!adminUser) {
                return res.status(400).send({
                    message: 'AdminUser not found'
                });
            }
            adminUser.displayName = req.body.displayName || adminUser.displayName;
            adminUser.email = req.body.email || adminUser.email;
            adminUser.save(function(err) {
                res.status(200).end();
            });
        });
    });

    /*
     |--------------------------------------------------------------------------
     | Log in with Email
     |--------------------------------------------------------------------------
     */
    app.post('/auth/login', function(req, res) {
        console.log("you login");
        AdminUser.findOne({
            email: req.body.email
        }, '+password', function(err, adminUser) {

            if (!adminUser) {
                return res.status(401).send({
                    message: 'Invalid email and/or password'
                });
            }
            adminUser.comparePassword(req.body.password, function (err, isMatch) {
                if (!isMatch) {
                    return res.status(401).send({
                        message: 'Invalid password'
                    });
                }
                if (!adminUser.isAdmin) {
                    return res.status(401).send({
                        message: "Hi There you shouldn't be here please get out of here ."
                    });
                }
                res.send({
                    token: createJWT(adminUser)
                });
            });
        });
    });

    /*
     |--------------------------------------------------------------------------
     | Create Email and Password Account
     |--------------------------------------------------------------------------
     */
    app.post('/auth/signup',ensureAuthenticated, function(req, res) {
        AdminUser.findOne({
            email: req.body.email
        }, function(err, existingUser) {
            if (existingUser) {
                return res.status(409).send({
                    message: 'adminUser name is already taken'
                });
            }
            var adminUser = new AdminUser({
                displayName: req.body.displayName,
                email: req.body.email,
                password: req.body.password
            });
            adminUser.save(function(err, result) {
                if (err) {
                    res.status(500).send({
                        message: err.message
                    });
                }
                res.send({
                    token: createJWT(result)
                });
            });
        });
    });
}
