var repository = require('../repository').Repository;
var suggestion = require('../models/suggestion-model');
var suggestionRepository = new repository(suggestion);
module.exports = function (app) {

  app.post('/addsuggestion',function(req,res){
    var su=new suggestion({
      needertype:req.body.kind,
      title:req.body.title,
      helptype:req.body.help ,
      details:req.body.details ,
      phone:req.body.phone ,
      address:req.body.address
    });
    suggestionRepository.add(su,function(err){
        if (err) {
            res.send(err);
        } else {
          res.render('MessageOk.pug');        }
    });

  });
  app.get('/suggest',function(req,res){
      res.render('suggest.pug');
  });
  app.get('/api/allsugs',function (req,res) {
      suggestion.find({},function (err,callback) {
        if (err) {
          console.log(err);
        }else {
          res.send(callback);
        }
      });
    });
    app.get('/api/allsugs2',function (req,res) {
        suggestion.find({},function (err,callback) {
          console.log(callback);
          if (err) {
            console.log(err);
          }else {
            res.render('donator-suggestions',{
              suggestions:callback
            });
          }
        });
      });
  app.post('/suggestion-api/deleteSug',function (req,res) {
          var id = req.param('id');
          console.log(id);
        suggestionRepository.remove({
            _id: id
              }, function(err) {
            if (err) {
                res.send(500);
              } else {
                res.json({ message: 'The Suggestion Successfully Deleted ' });

            }
        });
  })


}
