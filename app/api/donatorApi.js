var repository=require('../repository').Repository;
var donatorModel=require('../models/donator-model');
var donatorRepository=new repository(donatorModel);
var moment=require('moment');


var multer=require('multer');
var path=require('path');
var uploadPath=path.resolve('public/frontend/uploadimages/');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, uploadPath)
  },
  filename: function (req, file, cb) {
      cb(null, file.originalname);
  }
});
var upload=multer({storage: storage});

module.exports = function (app) {


  app.post('/donator_info',upload.single('profileimage'),function (req,res) {

    var obj=req.body;
    var newdonator={
           firstname:req.body.firstname,
           lastname:req.body.lastname,
           phone:req.body.phone,
           job:req.body.job,
    };
        if (req.file) {
            newdonator.profileimage=req.file.originalname;
        }else{
            newdonator.profileimage="default.png";
        }

    var donate = new donatorModel(newdonator);

    donatorRepository.add(donate,function(err){
        if (err) {
            res.send(err);
        } else {
            res.render('donator-profile');
        }
    })
  });
  app.get('/donator_api/allDonators',function (req,res) {
          donatorRepository.find({},function (callback,err) {
            if (err) {
              console.log(err);
            }else if (callback) {
              res.send(callback);

            }
          });
  });
  app.post('/donator_api/deleteDonator',function (req,res) {
          var id = req.param('id');
          console.log(id);
        donatorRepository.remove({
            _id: id
              }, function(err) {
            if (err) {
                res.send(500);
              } else {
                res.json({ message: 'The Donator Successfully Deleted ' });

            }
        });
  })

};
