var mongoose = require('mongoose');

// define the schema for our user model
var suggestSchema = mongoose.Schema({

  Id:{
      type:mongoose.Schema.Types.ObjectId
  },
  charityId:{
      type:mongoose.Schema.Types.ObjectId
  },
  title:{
    type:String
  },
  needertype:{
    type:String
  },
  helptype:{
    type:String
  },
  details:{
    type:String
  },
  phone:{
     type:String
  },
  address:{
    type:String
  }

});
module.exports=mongoose.model('suggestion', suggestSchema);
