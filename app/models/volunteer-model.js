var mongoose = require("mongoose");

var volunteerSchema = mongoose.Schema({
  Id: {
    type: mongoose.Schema.Types.ObjectId
  },
  firstname: {
    type: String
  },
  lastname: {
    type: String
  },
  dateofbirth: {
    type: Date
  },
  phone: {
    type: String
  },
  university: {
    type: String
  },
  studyfield: {
    type: String
  },
  address: {
    type: String
  },
  events: [
    {
      eventid: mongoose.Schema.Types.ObjectId,
      subscribed_tasks: [{ taskid: mongoose.Schema.Types.ObjectId }]
    }
  ],
  job: {
    type: String
  },
  bio: {
    type: String
  },
  skills: {
    type: String
  },
  profileimage:{
    type: String
  },
  charities: [{charid: mongoose.Schema.Types.ObjectId}]
});
module.exports = mongoose.model("volunteer", volunteerSchema);
