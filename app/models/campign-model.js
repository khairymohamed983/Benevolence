var mongoose = require('mongoose');

// define the schema for our user model
var compaignSchema = mongoose.Schema({

  Id:{
      type:mongoose.Schema.Types.ObjectId
  },
  suggestId:{
      type:mongoose.Schema.Types.ObjectId
  },
  title:{
    type:String
  },
  helptype:{
    type:String
  },
  details:{
    type:String
  },
  amount:{
     type:Number
  }

});
module.exports=mongoose.model('compaign', compaignSchema);
