var mongoose = require("mongoose");

var charitySchema = mongoose.Schema({
  Id: {
    type: mongoose.Schema.Types.ObjectId
  },
  fullname: {
    type: String
  },
  phone: {
    type: String
  },
  address: {
    type: String
  },
  register: {
    type: String
  },
  registernumber: {
    type: String
  },
  status: {
    type: Boolean
  },
  events: [
    {
      eventid: mongoose.Schema.Types.ObjectId,
      subscribed_tasks: [{ taskid: mongoose.Schema.Types.ObjectId }]
    }
  ],
  profileimage:{
    type: String
  },
  volunteers: [{volid: mongoose.Schema.Types.ObjectId}],
  
});
module.exports = mongoose.model("charity", charitySchema);
