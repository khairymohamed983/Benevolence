var mongoose = require("mongoose");

var donatorSchema = mongoose.Schema({
  Id: {
    type: mongoose.Schema.Types.ObjectId
  },
  firstname: {
    type: String
  },
  lastname: {
    type: String
  },
  phone: {
    type: String
  },
  events: [
    {
      eventid: mongoose.Schema.Types.ObjectId,
      subscribed_tasks: [{ taskid: mongoose.Schema.Types.ObjectId }]
    }
  ],
  job: {
    type: String
  },
  profileimage:{
    type: String
  }
});
module.exports = mongoose.model("donator", donatorSchema);
