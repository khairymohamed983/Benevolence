var mongoose = require('mongoose');

// define the schema for our user model
var taskSchema = mongoose.Schema({

  Id:{
      type:mongoose.Schema.Types.ObjectId
  },
  suggestId:{
      type:mongoose.Schema.Types.ObjectId
  },
  ownerId:{
      type:mongoose.Schema.Types.ObjectId
  },
  volunteers:{
      [type:mongoose.Schema.Types.ObjectId]
  },
  title:{
    type:String
  },
  to:{
    type:String
  },
  details:{
    type:String
  },
  amount:{
     type:Number
  }

});
module.exports=mongoose.model('task', taskSchema);
