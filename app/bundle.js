// Call any new api files here .
// this wrapper created by khairy ...
module.exports = function(app) {
// on connect to the database start seed the data
require('./seeder')(app);
// Auth API Routes
require('./api/adminUser')(app);
// application data API
require('./api/compaignApi')(app);
require('./api/suggestionApi')(app);
require('./api/volunteerApi')(app);
require('./api/charityApi')(app);
require('./api/donatorApi')(app);
// Application 'CLient' Routes #basic Routes
require('./routes')(app);
require('./authRoute')(app);
}
