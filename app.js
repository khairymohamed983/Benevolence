var path = require('path');
var qs = require('querystring');
var cors = require('cors');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var mongoose = require('mongoose');
var moment = require('moment');
var jwt = require('jwt-simple');
var passport = require('passport');
var config = require('./config/config');
var app = express();

app.set('view engine', 'pug');
app.set('view options', { layout: false });
app.set('views', path.join(__dirname, '/public/frontend/views/'));

var mongoOptions = {
    server: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } }
};

mongoose.connect(config.online, mongoOptions);
// mongoose.connect('mongodb://localhost:27017/charity');
//parse application/json

app.set('port', (process.env.PORT || 8000));
//app.set('host', process.env.NODE_IP || 'localhost');
app.use(cors());
app.use(session({
    secret: 'mysecret',
    cookie: {
        maxAge: 999999999,
        path: '/'
    },
    resave: true,
    saveUninitialized: true
}));

app.use(bodyParser.json());

//parse application/vnd.api+json as json
app.use(bodyParser.json({
    type: 'application/vnd.api+json'
}));

//parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}));

require('./config/passport')(passport); // pass passport for configuration
app.use(passport.initialize());
app.use(passport.session());

//override with X-HTTP-Method-override header in the request
app.use(methodOverride('X-HTTP-Method-Override'));
// Force HTTPS on Heroku
if (app.get('env') === 'production') {
    app.use(function (req, res, next) {
        var protocol = req.get('x-forwarded-proto');
        protocol == 'https' ? next() : res.redirect('https://' + req.hostname + req.url);
    });
}

app.use('/admin', express.static('public/backend'));
app.use('/', express.static('public/frontend'));
// bundle all our files here
require('./app/bundle')(app);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    // On Connecting to db start running the app
    app.listen(app.get('port'), function () {
        console.log('Node app is running on port', app.get('port'));
    });
});
exports = module.exports = app;