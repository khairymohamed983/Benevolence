angular.module('itcapp').controller('volctrl', ['$scope', '$http', '$modal', 'pinesNotifications', function($scope, $http, $modal, pinesNotifications) {
          $scope.allVols = [];
          $scope.loadData = function () {
            $http.get('/vol_api/allvols').success(function(data) {
                $scope.allVols = data;
                console.log(data);

            });
          }
          $scope.delete = function(vol) {
                 console.log(vol);
                 var modalInstance = $modal.open({
                     templateUrl: 'deleteModel.html',
                     controller: function($scope, $modalInstance) {
                         $scope.ok = function() {
                             $modalInstance.close();
                         };
                         $scope.cancel = function() {
                             $modalInstance.dismiss('cancel');
                         };
                     }
                 });
                 modalInstance.result.then(function() {
                         console.log(vol);
                         $http.post('/vol_api/deleteVols?id=' + vol._id).success(function(data) {
                             var index = $scope.allVols.indexOf(vol);
                             $scope.allVols.splice(index, 1);
                             pinesNotifications.notify({
                                 title: 'Delete Operation Successed',
                                 text: 'Volunteer ' + vol.firstname + ' Deleted Successfully .',
                                 type: 'info'
                             });
                             // Show the toast to the user
                         }).error(function(error) {
                             pinesNotifications.notify({
                                 title: 'Can\'t Delete vol',
                                 text: 'Volunteer ' + vol.firstname + ' Can\t Deleted .',
                                 type: 'error'
                             });
                         });
                     },
                     function() {});
                 //$scope.getData();

             };
          $scope.loadData();

}]);
