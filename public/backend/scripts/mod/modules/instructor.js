angular.module('itcapp').controller('InstructorsCtrl', ['$scope', '$http', '$modal', 'pinesNotifications', function($scope, $http, $modal, pinesNotifications) {
    $scope.allInstructores = [];
    $scope.txt = '';
    $scope.branches = [];
    $scope.getData = function() {
        $http.get('/api/instructores/all').success(function(data) {
            $scope.allInstructores = data;
        });
    };

    $scope.loadBranches = function() {
        $http.get('/api/branches/all').success(function(data) {
            $scope.branches = data;
            $scope.getData();
        });
    }

    $scope.getBranchName = function(id) {
        for (var i = 0; i < $scope.branches.length; i++) {
            if ($scope.branches[i]._id == id) {
                return $scope.branches[i].name;
            }
        }
    }
    $scope.delete = function(branch) {
        var modalInstance = $modal.open({
            templateUrl: 'deleteModel.html',
            controller: function($scope, $modalInstance) {
                $scope.ok = function() {
                    $modalInstance.close();
                };
                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                };
            }
        });

        modalInstance.result.then(function() {
                console.log(branch);
                $http.post('/api/Instructores/delete?id=' + branch._id).success(function(data) {
                    var index = $scope.allInstructores.indexOf(branch);
                    $scope.allInstructores.splice(index, 1);
                    pinesNotifications.notify({
                        title: 'Delete Operation Successed',
                        text: 'Instructor ' + branch.name + ' Deleted Successfully .',
                        type: 'info'
                    });
                    // Show the toast to the user
                }).error(function(error) {
                    pinesNotifications.notify({
                        title: 'Can\'t Delete Instructor',
                        text: 'Instructor ' + branch.name + ' Can\t Deleted .',
                        type: 'error'
                    });
                });;
            },
            function() {});
    };
    $scope.loadBranches();

}]);

angular.module('itcapp').controller('addInstructorCtrl', ['$scope', '$http', '$modal', 'pinesNotifications', '$location',
    function($scope, $http, $modal, pinesNotifications, $location) {
        $scope.instructor = {};
        $scope.branches = [];
        $scope.add = function() {
            console.log($scope.instructor);
            $http.post('/api/instructores/add', $scope.instructor).success(function(res) {
                pinesNotifications.notify({
                    title: 'Instructor Saved',
                    text: 'Instructor ' + $scope.instructor.name + ' Saved Successfully .',
                    type: 'success'
                });
                $location.path('/instructor');
            });
        }

        $scope.loadBranches = function() {
            $http.get('/api/branches/all').success(function(data) {
                $scope.branches = data;
            });
        }

        $scope.loadBranches();

    }
]);

angular.module('itcapp').controller('editInstructorCtrl', ['$scope', '$http', '$modal', 'pinesNotifications', '$location',

    function($scope, $http, $modal, pinesNotifications, $location) {
        $scope.branch = {};
        $scope.loadInstructor = function() {
            var id = ($location.search()).id;
            $http.get('/api/Instructors/single?id=' + id).success(function(data) {
                $scope.branch = data;
            });
        }
        $scope.add = function() {
            $http.post('/api/Instructors/update?id=' + $scope.branch._id, $scope.branch).success(function(res) {
                pinesNotifications.notify({
                    title: 'Instructor Saved',
                    text: 'Instructor ' + $scope.branch.name + ' Saved Successfully .',
                    type: 'success'
                });
                $location.path('/Instructors');
            });
        }
        $scope.loadInstructor();
    }
]);
