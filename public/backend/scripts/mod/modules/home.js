angular.module('itcapp')
    .controller('HomeCtrl', function($scope, $http, $theme) {
        'user strict';
        $scope.largeTiles = [{
            title: 'Packages',
            titleBarInfo: '55 Track',
            text: '22',
            color: 'info',
            classes: 'fa fa-shopping-cart'
        }, {
            title: 'Instructors',
            titleBarInfo: '5 Active',
            text: '15',
            color: 'orange',
            classes: 'glyphicon glyphicon-user'
        }, {
            title: 'Reservations',
            titleBarInfo: '5 Today',
            text: '150',
            color: 'danger',
            classes: 'fa fa-money'
        }, {
            title: 'Students',
            titleBarInfo: '15 New',
            text: '250',
            color: 'midnightblue',
            classes: 'fa fa-group'
        }, {
            title: 'Groups',
            titleBarInfo: '25 Active',
            text: '50',
            color: 'green',
            classes: 'fa fa-comments'
        }, {
            title: 'Student Attendence',
            titleBarInfo: '20 Attended',
            text: '250',
            color: 'success',
            classes: 'fa fa-check-square'
        }, {
            title: 'Branches',
            titleBarInfo: '2 new Opened',
            text: '5',
            color: 'primary',
            classes: 'fa fa-eye'
        }, {
            title: 'Time Table',
            titleBarInfo: 'Time Calender',
            text: 'View Tble',
            color: 'inverse',
            classes: 'fa fa-table'
        }];
    });
