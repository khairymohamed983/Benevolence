angular.module('itcapp')
  .controller('LoginCtrl', function ($scope, $location, $auth, $theme,$http) {

    $theme.set('fullscreen', true);

    $scope.$on('$destroy', function () {
      $theme.set('fullscreen', false);
    });

    $scope.login = function () {
      $auth.login($scope.user)
        .then(function () {
          // toastr.success('You have successfully signed in!');
          $location.path('/admin');
        })
        .catch(function (error) {
          // toastr.error(error.data.message, error.status);
        });
    };
    $scope.authenticate = function (provider) {
      $auth.authenticate(provider)
        .then(function () {
          // toastr.success('You have successfully signed in with ' + provider + '!');
          $location.path('/admin');
        })
        .catch(function (error) {
          if (error.message) {
            // Satellizer promise reject error.
            // toastr.error(error.message);
          } else if (error.data) {
            // HTTP response error from server
            // toastr.error(error.data.message, error.status);
          } else {
            // toastr.error(error);
          }
        });
    };
  });
