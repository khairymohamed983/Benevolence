angular.module('itcapp').controller('packageCtrl', ['$scope', '$http', '$modal', 'pinesNotifications', function($scope, $http, $modal, pinesNotifications) {
    $scope.allPackages = [];
    $scope.getData = function() {
        $http.get('/api/courses/all').success(function(data) {
            $scope.allPackages = data;
        });
    };

    $scope.deletePackage = function(package) {
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: function($scope, $modalInstance, items) {
                $scope.ok = function() {
                    $modalInstance.close(true);
                };
                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve: {
                items: function() {
                    return $scope.items;
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {
            if (selectedItem) {
                $http.post('/api/courses/delete?id=' + package._id).success(function(data) {
                    var index = $scope.allPackages.indexOf(package);
                    $scope.allPackages.splice(index, 1);
                    pinesNotifications.notify({
                        title: 'Delete Operation Successed',
                        text: 'Package ' + package.name + ' Deleted Successfully .',
                        type: 'info'
                    });
                    // Show the toast to the user
                }).error(function(error) {
                    pinesNotifications.notify({
                        title: 'Can\'t Delete Package',
                        text: 'Package ' + package.name + ' Can\t Deleted .',
                        type: 'error'
                    });
                });;
            }
        }, function() {});
    };
    $scope.getData();
}]);

angular.module('itcapp').controller('editpackageCtrl', ['$scope', '$http', '$modal', '$location', 'pinesNotifications', function($scope, $http, $modal, $location, pinesNotifications) {
    $scope.package = {
        name: 'fsdfsdfsd',
        hours: 0,
        tracks: []
    };
    $scope.track = {
        name: '',
        hours: 0
    }
    $scope.bTrack = {};
    $scope.isEdit = false;
    $scope.loadPackage = function() {
        $http.get('/api/courses/single?id=' + ($location.search()).id).success(function(data) {
            $scope.package = data;
        });
    }

    $scope.$watchCollection('package.tracks', function(trck) {
        if (trck.length) {
            $scope.calcTracks();
        }
    });

    $scope.calcTracks = function() {
        $scope.package.hours = 0;
        for (var i = 0; i < $scope.package.tracks.length; i++) {
            $scope.package.hours += $scope.package.tracks[i].hours;
        }
    }
    $scope.addPackage = function() {
        $scope.calcTracks();
        $http.post('/api/courses/update?id=' + $scope.package._id, $scope.package).then(function(res) {
            if (res.status == 200) {
                pinesNotifications.notify({
                    title: 'Package Saved',
                    text: 'Package ' + $scope.package.name + ' Updated and Saved Successfully .',
                    type: 'success'
                });
                $location.path('/packages');
            } else {
                pinesNotifications.notify({
                    title: 'Can\'t Update Package',
                    text: 'Package ' + $scope.package.name + ' Can\t Saved .',
                    type: 'error'
                });
            }
        });
    };

    $scope.addTrack = function() {
        $scope.package.tracks.push($scope.track);
        $scope.track = {
            name: '',
            hours: 0
        }
    }

    $scope.editTrack = function(track) {
        $scope.track = track;
        $scope.bTrack = track;
        $scope.isEdit = true;
    }

    $scope.saveTrack = function() {
        $scope.isEdit = false;
        $scope.calcTracks();
        $scope.track = {
            name: '',
            hours: 0
        }
    }

    $scope.cancel = function() {
        $scope.track = $scope.bTrack;
        $scope.saveTrack();
    }

    $scope.removeTrack = function(track) {
        var index = $scope.package.tracks.indexOf(track);
        $scope.package.tracks.splice(track, 1);
    }

    $scope.loadPackage();
}]);

angular.module('itcapp').controller('addpackageCtrl', ['$scope', '$http', '$modal', '$location', 'pinesNotifications', function($scope, $http, $modal, $location, pinesNotifications) {
    $scope.package = {
        name: '',
        hours: 0,
        tracks: []
    };

    $scope.track = {
        name: '',
        hours: 0
    }
    $scope.$watchCollection('package.tracks', function(trck) {
        if (trck.length) {
            $scope.package.hours = 0;
            for (var i = 0; i < $scope.package.tracks.length; i++) {
                $scope.package.hours += $scope.package.tracks[i].hours;
            }
        }
    });

    $scope.addPackage = function() {
        $http.post('/api/courses/add', $scope.package).then(function(res) {
            if (res.status == 200) {
                pinesNotifications.notify({
                    title: 'Package Saved',
                    text: 'Package ' + $scope.package.name + ' Saved Successfully .',
                    type: 'success'
                });
                $location.path('/packages');
            } else {
                pinesNotifications.notify({
                    title: 'Can\'t add Package',
                    text: 'Package ' + $scope.package.name + ' Can\t Saved .',
                    type: 'error'
                });
            }
        });
    };

    $scope.addTrack = function() {
        $scope.package.tracks.push($scope.track);
        $scope.track = {
            name: '',
            hours: 0
        }
    }

    $scope.removeTrack = function(track) {
        var index = $scope.package.tracks.indexOf(track);
        $scope.package.tracks.splice(track, 1);
    }
}]);
