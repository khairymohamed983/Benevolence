angular.module('itcapp')
    .controller('ProfileCtrl', function($scope, $auth, pinesNotifications, Account) {

        $scope.getProfile = function() {
            Account.getProfile()
                .then(function(response) {
                    $scope.user = response.data;
                })
                .catch(function(response) {
                    $http.post('/api/branches/add', $scope.branch)
                        .success(function(res) {
                            pinesNotifications.notify({
                                title: response.status,
                                text: response.data.message,
                                type: 'error'
                            });
                        });
                });
        }

        $scope.updateProfile = function() {
            Account.updateProfile($scope.user)
                .then(function() {
                    pinesNotifications.notify({
                        title: 'Operation Successed',
                        text: 'Profile has been updated',
                        type: 'success'
                    });
                })
                .catch(function(response) {
                    pinesNotifications.notify({
                        title: response.status,
                        text: response.data.message,
                        type: 'error'
                    });
                });
        };

        $scope.getProfile();
    });
