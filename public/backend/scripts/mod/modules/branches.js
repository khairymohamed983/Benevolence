angular.module('itcapp').controller('sugctrl', ['$scope', '$http', '$modal', 'pinesNotifications', function($scope, $http, $modal, pinesNotifications) {
    $scope.allBranches = [];
    $scope.txt = '';
    $scope.getData = function() {
        $http.get('/api/branches/all').success(function(data) {
            $scope.allBranches = data;
            console.log(data);
        });
    };

    $scope.delete = function(branch) {
        var modalInstance = $modal.open({
            templateUrl: 'deleteModel.html',
            controller: function($scope, $modalInstance) {
                $scope.ok = function() {
                    $modalInstance.close();
                };
                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function() {
                console.log(branch);
                $http.post('/api/branches/delete?id=' + branch._id).success(function(data) {
                    var index = $scope.allBranches.indexOf(branch);
                    $scope.allBranches.splice(index, 1);
                    pinesNotifications.notify({
                        title: 'Delete Operation Successed',
                        text: 'Branch ' + branch.name + ' Deleted Successfully .',
                        type: 'info'
                    });
                    // Show the toast to the user
                }).error(function(error) {
                    pinesNotifications.notify({
                        title: 'Can\'t Delete Branch',
                        text: 'Branch ' + branch.name + ' Can\t Deleted .',
                        type: 'error'
                    });
                });
            },
            function() {});
    };
    $scope.getData();
}]);


angular.module('itcapp').controller('addBranchCtrl', ['$scope', '$http', '$modal', 'pinesNotifications', '$location',
    function($scope, $http, $modal, pinesNotifications, $location) {
        $scope.branch = {};
        $scope.add = function() {
            $http.post('/api/branches/add', $scope.branch).success(function(res) {
                pinesNotifications.notify({
                    title: 'Branch Saved',
                    text: 'Branch ' + $scope.branch.name + ' Saved Successfully .',
                    type: 'success'
                });
                $location.path('/branches');
            });
        }
    }
]);

angular.module('itcapp').controller('editBranchCtrl', ['$scope', '$http', '$modal', 'pinesNotifications', '$location',
    function($scope, $http, $modal, pinesNotifications, $location) {
        $scope.branch = {};
        $scope.loadBranch = function() {
            var id = ($location.search()).id;
            $http.get('/api/branches/single?id=' + id).success(function(data) {
                $scope.branch = data;
            });
        }
        $scope.add = function() {
            $http.post('/api/branches/update?id=' + $scope.branch._id, $scope.branch).success(function(res) {
                pinesNotifications.notify({
                    title: 'Branch Saved',
                    text: 'Branch ' + $scope.branch.name + ' Saved Successfully .',
                    type: 'success'
                });
                $location.path('/branches');
            });
        }
        $scope.loadBranch();
    }
]);
