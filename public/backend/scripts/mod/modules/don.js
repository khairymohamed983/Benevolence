angular.module('itcapp').controller('donctrl', ['$scope', '$http', '$modal', 'pinesNotifications', function($scope, $http, $modal, pinesNotifications) {
          $scope.allDonators = [];
          $scope.loadData = function () {
            $http.get('/donator_api/allDonators').success(function(data) {
                $scope.allDonators = data;
                console.log(data);

            });
          }

        // the crud of the class
         $scope.delete = function(Donator) {
                 console.log(Donator);
                 var modalInstance = $modal.open({
                     templateUrl: 'deleteModel.html',
                     controller: function($scope, $modalInstance) {
                         $scope.ok = function() {
                             $modalInstance.close();
                         };
                         $scope.cancel = function() {
                             $modalInstance.dismiss('cancel');
                         };
                     }
                 });
                 modalInstance.result.then(function() {
                         console.log(Donator);
                         $http.post('/donator_api/deleteDonator?id=' + Donator._id).success(function(data) {
                             var index = $scope.allDonators.indexOf(Donator);
                             console.log(index);
                             $scope.allDonators.splice(index, 1);
                             pinesNotifications.notify({
                                 title: 'Delete Operation Successed',
                                 text: 'Donator ' + Donator.title + ' Deleted Successfully .',
                                 type: 'info'
                             });
                             // Show the toast to the user
                         }).error(function(error) {
                             pinesNotifications.notify({
                                 title: 'Can\'t Delete Donator',
                                 text: 'Donator ' + Donator.title + ' Can\t Deleted .',
                                 type: 'error'
                             });
                         });
                     },
                     function() {});
                 //$scope.getData();

             };
        $scope.loadData();

}]);
