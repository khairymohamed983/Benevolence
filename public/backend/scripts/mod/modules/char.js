angular.module('itcapp').controller('charctrl', ['$scope', '$http', '$modal', 'pinesNotifications', function($scope, $http, $modal, pinesNotifications) {
        $scope.allCharities = [];
        $scope.all=true;
        $scope.accepted=false;
        $scope.refused=false;
        //this the navigation between components
        $scope.expendToAccept = function ($index) {

                  console.log('expending to accepted .....  ');
                  $scope.accepted = true ;
                  $scope.all = false;
                  $scope.refused = false ;

             }
        $scope.expendToAll = function ($index) {
                  console.log('expending to All .....  ');
                  $scope.all = true ;
                  $scope.accepted = false ;
                  $scope.refused = false ;


             }
        $scope.expendToRefused = function ($index) {
                  console.log('expending to Refused .....  ');
                  $scope.refused= true ;
                  $scope.all = false ;
                  $scope.accepted = false ;


             }

        $scope.loadData = function () {
          $http.get('/charity-api/allCharities').success(function(data) {
              $scope.allCharities = data;
              for (var i = 0; i < $scope.allCharities.length; i++) {
                console.log($scope.allCharities[i].status);
              }
          });
        }
        $scope.delete = function(charity) {
                  console.log(charity);
                  var modalInstance = $modal.open({
                      templateUrl: 'deleteModel.html',
                      controller: function($scope, $modalInstance) {
                          $scope.ok = function() {
                              $modalInstance.close();
                          };
                          $scope.cancel = function() {
                              $modalInstance.dismiss('cancel');
                          };
                      }
                  });
                  modalInstance.result.then(function() {
                          console.log(charity._id);
                          $http.post('/charity-api/deleteCharity?id=' + charity._id).success(function(data) {
                              var index = $scope.allCharities.indexOf(charity);
                              $scope.allCharities.splice(index, 1);
                              pinesNotifications.notify({
                                  fullname: 'Delete Operation Successed',
                                  text: 'charity ' + charity.fullname + ' Deleted Successfully .',
                                  type: 'info'
                              });
                              // Show the toast to the user
                          }).error(function(error) {
                              pinesNotifications.notify({
                                  fullname: 'Can\'t Delete charity',
                                  text: 'charity ' + charity.fullname + ' Can\t Deleted .',
                                  type: 'error'
                              });
                          });
                      },
                      function() {});
                  //$scope.getData();

              };
        // the crud of the charity
        $scope.refuse=function (charity) {
          var newStatus={status:false};
          $http.post('/charity-api/UpdateCharity?id=' + charity._id,newStatus).success(function() {
                    pinesNotifications.notify({
                        fullname: 'Refuse Operation Successed',
                        text: 'charity ' + charity.fullname + ' Refused Successfully .',
                        type: 'info'
                    });
                    $scope.loadData();
                });
        }
        $scope.accepte=function (charity) {
          var newStatus={status:true};
          $http.post('/charity-api/UpdateCharity?id=' + charity._id,newStatus).success(function() {
                    pinesNotifications.notify({
                        fullname: 'Accepte Operation Successed',
                        text: 'charity ' + charity.fullname + ' Accepted Successfully .',
                        type: 'info'
                    });
                    $scope.loadData();
                });

        }

        $scope.loadData();

}]);
