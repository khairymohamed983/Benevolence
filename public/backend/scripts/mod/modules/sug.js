angular.module('itcapp').controller('sugctrl', ['$scope', '$http', '$modal', 'pinesNotifications', function($scope, $http, $modal, pinesNotifications) {
          $scope.allsugs = [];
          $scope.newSug={};


          $scope.loadData = function () {
            $http.get('/api/allsugs').success(function(data) {
                $scope.allsugs = data;
                console.log(data);

            });
          }

        // the crud of the sug
         $scope.addSug=function () {

           $http.post('/suggestion-api/addSug',$scope.newSug).success(function () {
               pinesNotifications.notify({
                   title: 'Suggestion Saved',
                   text: 'Suggestion ' + $scope.newSug.title + ' Saved Successfully .',
                   type: 'success'
               });
                  $('#addSugModal').modal('toggle');
                 $scope.loadData();
           });
         }
         $scope.delete = function(sug) {
                 console.log(sug);
                 var modalInstance = $modal.open({
                     templateUrl: 'deleteModel.html',
                     controller: function($scope, $modalInstance) {
                         $scope.ok = function() {
                             $modalInstance.close();
                         };
                         $scope.cancel = function() {
                             $modalInstance.dismiss('cancel');
                         };
                     }
                 });
                 modalInstance.result.then(function() {
                         console.log(sug);
                         $http.post('/suggestion-api/deleteSug?id=' + sug._id).success(function(data) {
                             var index = $scope.allsugs.indexOf(sug);
                             $scope.allsugs.splice(index, 1);
                             pinesNotifications.notify({
                                 title: 'Delete Operation Successed',
                                 text: 'sug ' + sug.title + ' Deleted Successfully .',
                                 type: 'info'
                             });
                             // Show the toast to the user
                         }).error(function(error) {
                             pinesNotifications.notify({
                                 title: 'Can\'t Delete sug',
                                 text: 'sug ' + sug.title + ' Can\t Deleted .',
                                 type: 'error'
                             });
                         });
                     },
                     function() {});
                 //$scope.getData();

             };



        $scope.loadData();

}]);
