angular
    .module('itcapp', [
        'oc.lazyLoad',
        'satellizer',
        'angularUtils.directives.dirPagination',
        'theme.core.services'
    ]);
