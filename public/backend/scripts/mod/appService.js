var app = angular.module('itcapp');

app.factory('$branchFactory', ['$http', function($http) {
    var promise = $http.get('/api/branches/all');
    return promise;
}]);
