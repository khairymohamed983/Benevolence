"use strict";
angular
    .module('themesApp', [
        'theme',
        'itcapp'
    ]).config(['$provide', '$routeProvider', '$authProvider', '$locationProvider', '$httpProvider',
        function($provide, $routeProvider, $authProvider, $locationProvider, $httpProvider) {
            // config the app to work without # By us html5Mode
            $locationProvider.html5Mode(true);

            var skipIfLoggedIn = function($q, $location, $auth) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    deferred.reject();
                    $location.path('/');
                } else {
                    deferred.resolve();
                }
                return deferred.promise;
            };

            var loginRequired = function($q, $location, $auth) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    deferred.resolve();
                } else {
                    $location.path('/admin/login');
                }
                return deferred.promise;
            };

            // NOTE: login has a custom route because it has a custome resolve (skipIfLoggedIn)
            // NOTE: logout has a custom route because it has no template so it can't be in the global route
            'use strict';
            $routeProvider.when('/admin', {
                templateUrl:function(param) {
                    var page = sessionStorage.getItem('page');
                    if (page == 0 || page == undefined || page == null) {
                      return '/admin/views/index.html';
                    }
                    sessionStorage.setItem("page", 0);
                    return '/admin/views/' + page + '.html';
                }
                // ,
                // resolve: {
                //     loginRequired: loginRequired
                // }
            });

            $routeProvider.when('/suggestions', {

                templateUrl: '/admin/views/sug.html'
                ,controller:'sugctrl'
                // ,
                // resolve: {
                //     skipIfLoggedIn: skipIfLoggedIn
                // }
            });
            $routeProvider.when('/volunteers', {

                templateUrl: '/admin/views/vol.html'
                ,controller:'volctrl'
                // ,
                // resolve: {
                //     skipIfLoggedIn: skipIfLoggedIn
                // }
            });
            $routeProvider.when('/donators', {

                templateUrl: '/admin/views/don.html'
                ,controller:'donctrl'
                // ,
                // resolve: {
                //     skipIfLoggedIn: skipIfLoggedIn
                // }
            });
            $routeProvider.when('/charities', {

                templateUrl: '/admin/views/char.html'
                ,controller:'charctrl'
                // ,
                // resolve: {
                //     skipIfLoggedIn: skipIfLoggedIn
                // }
            });
            $routeProvider.when('/admin/login', {
                  templateUrl: 'admin/views/login.html',
                  resolve: {
                      skipIfLoggedIn: skipIfLoggedIn
                  }
              });

            $routeProvider.when('/admin/logout', {
                template: null,
                controller: 'LogoutCtrl',
                resolve: {
                    loginRequired: loginRequired
                }
            });
            $routeProvider.when('/admin/:templateFile*', {
                templateUrl: function(param) {
                    return '/admin/views/' + param.templateFile + '.html';
                }
                // ,
                // resolve: {
                //     loginRequired: loginRequired
                // }
            });
            $routeProvider.otherwise({
                redirectTo: '/admin/404'
            });
        }
    ]);
